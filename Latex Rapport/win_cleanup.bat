@echo off
del /S *.out
del /S *.aux
del /S *.bbl
del /S *.blg
del /S *.synctex
del /S *.toc
del /S *.log
del /S *.gz
del /S *.gz(busy)