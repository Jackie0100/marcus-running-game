﻿using UnityEngine;
using System.Collections;

public class TabMenu : MonoBehaviour
{
    [SerializeField]
    Sprite activeTab;
    [SerializeField]
    Sprite inactiveTab;
    [SerializeField]
    GameObject buttonPanel;

    [SerializeField]
    float offset = 1920;
    [SerializeField]
    float screenWidth = 1920;
    int index = 0;
    bool touchReleased = true;


	// Use this for initialization
	void Awake()
    {
        toPos = offset;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (!Mathf.Approximately(toPos, this.GetComponent<RectTransform>().localPosition.x))
        {
            this.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Lerp(this.GetComponent<RectTransform>().localPosition.x, toPos, 0.25f), this.GetComponent<RectTransform>().localPosition.y, this.GetComponent<RectTransform>().localPosition.z);
        }
        if (Input.touchCount >= 1 && touchReleased)
        {
            if (Input.touches[0].deltaPosition.y >= 1)
            {
                return;
            }
            if (Input.touches[0].deltaPosition.x >= 20)
            {
                if (index == 0)
                {
                    return;
                }
                else
                {
                    LerpToIndex(index - 1);
                    touchReleased = false;
                }
            }
            if (Input.touches[0].deltaPosition.x <= -20)
            {
                if (index == 2)
                {
                    return;
                }
                else
                {
                    LerpToIndex(index + 1);
                    touchReleased = false;
                }
            }
        }
        else if (Input.touchCount == 0 && !touchReleased)
        {
            touchReleased = true;
        }
        
	}
    
    float toPos;

    public void LerpToIndex(int index)
    {
        buttonPanel.transform.GetChild(this.index).GetComponent<UnityEngine.UI.Image>().sprite = inactiveTab;
        this.index = index;
        buttonPanel.transform.GetChild(this.index).GetComponent<UnityEngine.UI.Image>().sprite = activeTab;
        toPos = offset - ((float)index * screenWidth);
    }
}
