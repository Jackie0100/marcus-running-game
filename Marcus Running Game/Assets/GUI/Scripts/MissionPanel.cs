﻿using UnityEngine;
using System.Collections;

public class MissionPanel : MonoBehaviour
{
    [SerializeField]
    float timer = 5;
    [SerializeField]
    float animateAt = 1;

    bool animationPlayed = false;

	// Use this for initialization
	void Start ()
    {
        this.GetComponent<Animation>().wrapMode = WrapMode.Once;
        this.GetComponent<Animation>().Play();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            this.gameObject.SetActive(false);
        }
        if (!animationPlayed && timer <= animateAt)
        {
            animationPlayed = true;
            this.GetComponent<Animation>()["StartAnimation"].speed = -1;
            this.GetComponent<Animation>()["StartAnimation"].time = this.GetComponent<Animation>()["StartAnimation"].length;
            this.GetComponent<Animation>().Play();
        }
    }

    public void ResetTimer(float time)
    {
        timer = time;
    } 
}
