﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class FPSCounter : MonoBehaviour
{
    [SerializeField]
    private float updateinterval = 0.5f;

    private float timer = 0;
    private int frames = 0;
    void Update()
    {
        timer += Time.deltaTime;
        frames++;
        if (timer > updateinterval)
        {
            this.GetComponent<Text>().text = "FPS: " + ((float)frames / updateinterval);
            timer = 0;
            frames = 0;
        }
    }
}
