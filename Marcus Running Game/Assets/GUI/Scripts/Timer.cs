﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class Timer : MonoBehaviour
{
    Text textlabel;
    [SerializeField]
    float timer = 5;
    [SerializeField]
    Animation textAnimation;
    [SerializeField]
    GameObject playerObject;
    [SerializeField]
    UnityEngine.UI.Button pauseButton;

    // Use this for initialization
    void Start()
    {
        textlabel = this.GetComponent<Text>();
        textAnimation.wrapMode = WrapMode.Loop;
        textAnimation.Play();
        playerObject.GetComponent<Player>().enabled = false;
        pauseButton.enabled = false;
    }

    // Update is called once per frame
    void Update ()
    {
	    if (timer > 0)
        {
            textlabel.text = Mathf.CeilToInt(timer).ToString();
            timer -= Time.deltaTime;
        }
        else
        {
            pauseButton.enabled = true;
            this.transform.parent.gameObject.SetActive(false);
            playerObject.GetComponent<Player>().enabled = true;
        }
    }

    public void ResetTimer(float time)
    {
        timer = time;
    }
}
