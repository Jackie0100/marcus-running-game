﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelLoader : MonoBehaviour
{
    [SerializeField]
    bool asyncLoad = false;
    [SerializeField]
    LoadSceneMode loadMode = LoadSceneMode.Single;
    [SerializeField]
    ThreadPriority loadPriority = ThreadPriority.Normal;
    [SerializeField]
    string level = "0";
    [SerializeField]
    bool useIndex = false;
    
    public void LoadLevel()
    {
        Application.backgroundLoadingPriority = loadPriority;
        if (asyncLoad)
        {
            if (useIndex)
            {
                SceneManager.LoadSceneAsync(int.Parse(level), loadMode);
            }
            else
            {
                SceneManager.LoadSceneAsync(level, loadMode);
            }
        }
        else
        {
            if (useIndex)
            {
                SceneManager.LoadScene(int.Parse(level), loadMode);
            }
            else
            {
                SceneManager.LoadScene(level, loadMode);
            }
        }
    }
}

