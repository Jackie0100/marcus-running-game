﻿using UnityEngine;
using System.Collections;

public class Obsticle : MonoBehaviour
{
    [SerializeField]
    protected float lifeloss = 1;
	// Use this for initialization
	public virtual void Start()
    {
	
	}
	
	// Update is called once per frame
	public virtual void Update()
    {
	
	}

    public virtual void CollisionEnter2D(Collision2D col, Player player)
    {
        player.Health -= lifeloss;
    }
}
