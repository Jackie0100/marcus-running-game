﻿using UnityEngine;
using System.Collections;

public class ButtonSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] fallingObjects;
    [SerializeField]
    float minSpawnTime = 0.5f;
    [SerializeField]
    float maxSpawnTime = 2;

    float spawnTimer = 0;
    float waitSpawn = 0;

	// Use this for initialization
	void Start ()
    {
        waitSpawn = Random.Range(minSpawnTime, maxSpawnTime);
	}
	
	// Update is called once per frame
	void Update ()
    {
        spawnTimer += Time.deltaTime;
        if (spawnTimer > waitSpawn)
        {
            GameObject go = Instantiate(fallingObjects[Random.Range(0, fallingObjects.Length)]);
            go.transform.parent = this.transform;
            go.transform.position = new Vector3(this.transform.position.x + Random.Range(-5.0f, 5.0f), this.transform.position.y);
            spawnTimer = 0;
            waitSpawn = Random.Range(minSpawnTime, maxSpawnTime);
        }
    }
}
