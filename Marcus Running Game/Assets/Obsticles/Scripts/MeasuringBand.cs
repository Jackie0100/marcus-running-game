﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class MeasuringBand : Obsticle
{
    [SerializeField]
    float jumpTime = 2;

    float jumptimer = 0;

    bool isJumping = false;

	// Use this for initialization
	public override void Start ()
    {
	
	}

    // Update is called once per frame
    public override void Update ()
    {
	    
	}

    public override void CollisionEnter2D(Collision2D col, Player player)
    {
        if (col.contacts.All(t => t.collider.transform.position.y < player.transform.position.y))
        {
            isJumping = true;
            StartCoroutine(JumpPlayer(player));
        }
        else
        {
            player.Health -= lifeloss;
        }
    }

    IEnumerator JumpPlayer(Player player)
    {
        while (isJumping && jumptimer < jumpTime)
        {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 10);
            jumptimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}