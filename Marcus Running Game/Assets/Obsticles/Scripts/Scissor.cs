﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Scissor : Obsticle
{
    [SerializeField]
    int pointsWorth = 10;

	[SerializeField]
	GameObject buttonParticle;

	// Use this for initialization
	public override void Start ()
    {
	
	}
	
	// Update is called once per frame
	public override void Update ()
    {
	
	}

    public override void CollisionEnter2D(Collision2D col, Player player)
    {
        if (col.contacts.All(t => t.collider.transform.position.y < player.transform.position.y))
        {
			Instantiate (buttonParticle.gameObject, this.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            player.Score += pointsWorth;
			player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (player.GetComponent<Rigidbody2D> ().velocity.x, 10);
		}
        else
        {
            player.Health -= lifeloss;
        }
    }
}
