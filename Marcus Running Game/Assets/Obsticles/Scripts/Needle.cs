﻿using UnityEngine;
using System.Collections;

public class Needle : Obsticle
{
    bool flipflop = false;
    float maxheight = 3;
    float minheight = -3;
    float speed = 5;
	
	// Update is called once per frame
	public override void Update ()
    {
        if (!flipflop)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (Time.deltaTime * speed));
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (Time.deltaTime * speed));
        }

        if (transform.position.y < minheight && flipflop)
        {
            flipflop = false;
        }
        else if (transform.position.y > maxheight && !flipflop)
        {
            flipflop = true;
        }
	}
}
