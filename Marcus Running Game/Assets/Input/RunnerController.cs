﻿using UnityEngine;
using System.Collections;
using System;

public class RunnerController : Controller
{
    private float maxJumpTime = 1;

    public float _minJumpForce = 5f;
    public float _maxJumpForce = 5f;

    public float speed = 5;

    public RunnerController(bool fixedupdate) : base (fixedupdate)
    {
    }

    public override void LeftAction(Player player)
    {
        HoldLeftTimer += Time.deltaTime;
        if (HoldLeftTimer <= maxJumpTime && player.isGrounded)
        {
            player.GetComponent<Animator>().SetInteger("State", 2);
            Jump(player);

        }
        else
        {
            player.GetComponent<Animator>().SetInteger("State", 3);
        }
    }

    public override void RightAction(Player player)
    {
        player.GetComponent<BoxCollider2D>().size = new Vector2(3f, 2f);
        player.GetComponent<Animator>().SetInteger("State", 4);
        player.playerSounds.clip = player.slide;
        player.playerSounds.Play();
    }

    public override void StopActionLeft(Player player)
    {
        player.GetComponent<Animator>().SetInteger("State", 3);
        player.isGrounded = false;
        HoldLeftTimer = 0;
    }

    public override void StopActionRight(Player player)
    {
        HoldRightTimer = 0;
        player.GetComponent<BoxCollider2D>().size = new Vector2(2f, 3f);
        player.GetComponent<Animator>().SetInteger("State", 1);
    }

    private void Jump(Player player)
    {
        player.playerSounds.clip = player.jump;
        player.playerSounds.Play();

        float verticalJumpForce = ((_maxJumpForce - _minJumpForce) * (HoldLeftTimer / maxJumpTime)) + _minJumpForce;
        if (verticalJumpForce > _maxJumpForce)
        {
            verticalJumpForce = _maxJumpForce;
        }
        Vector2 resolvedJump = new Vector2(player.GetComponent<Rigidbody2D>().velocity.x, verticalJumpForce);
        player.GetComponent<Rigidbody2D>().velocity = resolvedJump;
    }

    public override void MovePlayer(Player player)
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(1 * speed, player.GetComponent<Rigidbody2D>().velocity.y);
    }
}
