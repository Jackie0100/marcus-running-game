﻿using UnityEngine;
using System.Collections;
using System;

public class DogdeController : Controller
{
    public DogdeController(bool fixedupdate) : base (fixedupdate)
    {
    }

    public override void LeftAction(Player player)
    {
        HoldLeftTimer += Time.deltaTime;
        LeftActionPlaying = true;
        if (player.isGrounded && !RightActionPlaying)
        {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 20);
        }
        player.isGrounded = false;
    }

    public override void MovePlayer(Player player)
    {
    }

    public override void RightAction(Player player)
    {
        HoldRightTimer += Time.deltaTime;
        RightActionPlaying = true;
        if (player.isGrounded && !LeftActionPlaying)
        {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 20);
        }

        player.isGrounded = false;
    }

    public override void StopActionLeft(Player player)
    {
        HoldLeftTimer = 0;
    }

    public override void StopActionRight(Player player)
    {
        HoldRightTimer = 0;
    }
}
