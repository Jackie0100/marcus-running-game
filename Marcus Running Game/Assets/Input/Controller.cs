﻿using UnityEngine;
using System.Collections;

public abstract class Controller
{
    public abstract void LeftAction(Player player);
    public abstract void RightAction(Player player);
    public abstract void StopActionLeft(Player player);
    public abstract void StopActionRight(Player player);
    public abstract void MovePlayer(Player player);
    public float HoldLeftTimer { get; set; }
    public float HoldRightTimer { get; set; }
    public bool FixedUpdate { get; set; }
    public bool LeftButtonHeldDown { get; set; }
    public bool RightButtonHeldDown { get; set; }
    public bool LeftActionPlaying { get; set; }
    public bool RightActionPlaying { get; set; }
    

    public Controller(bool fixedupdate)
    {
        FixedUpdate = fixedupdate;
    }

    public virtual void MoveCamera(Player player)
    {
        player.mainCamera.transform.position = new Vector3(player.transform.position.x + 5f, player.transform.position.y, -100);
    }
}
