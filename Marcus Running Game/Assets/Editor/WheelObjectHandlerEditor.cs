﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WheelObjectHandler))]
public class WheelObjectHandlerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WheelObjectHandler myScript = (WheelObjectHandler)target;
        if (GUILayout.Button("Build Wheel"))
        {
            myScript.BuildWheel();
        }
    }
}