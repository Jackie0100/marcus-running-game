﻿using UnityEngine;
using System.Collections;

public class WheelObjectHandler : MonoBehaviour
{
    [SerializeField]
    Sprite[] wheelIcons;
    [SerializeField]
    int[] wheelIconsIndex;
    [SerializeField]
    int NoOfObjects;
    [SerializeField]
    float offsetAngle;
    [SerializeField]
    float offsetX;
    [SerializeField]
    float offsetY;
    [SerializeField]
    float radius;
    [SerializeField]
    float spriteSize = 1;


    float diffrenceAngle
    {
        get { return 360.0f / (float)NoOfObjects; }
    }

    public void BuildWheel()
    {
        while (this.transform.childCount != 0)
        {
            DestroyImmediate(this.transform.GetChild(0).gameObject);
        }

        GameObject go;

        for (float i = 0; i < NoOfObjects; i += 1)
        {
            go = new GameObject();
            go.name = "OBJ: " + i;
            go.transform.parent = this.transform;
            go.transform.localPosition = new Vector2((float)(radius * Mathf.Cos(((i * diffrenceAngle) + offsetAngle) * (Mathf.PI / 180.0f))) + offsetX, (float)(radius * Mathf.Sin(((i * diffrenceAngle) + offsetAngle) * (Mathf.PI / 180.0f))) + offsetY);
            go.transform.rotation = Quaternion.Euler(0, 0, (i * diffrenceAngle) + offsetAngle + 90);
            go.transform.localScale = new Vector3(spriteSize, spriteSize, spriteSize);
            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = wheelIcons[wheelIconsIndex[(int)i]];
            sr.sortingOrder = 5;
        }
    }

    public int GetIconRotationIndex(float rot)
    {
        return wheelIconsIndex[Mathf.Abs(Mathf.FloorToInt((rot - 60) / diffrenceAngle))];
    }
}