﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpinScreen : MonoBehaviour
{
    [SerializeField]
    int spinLeft = 3;
    [SerializeField]
    Text spinLeftText;
    [SerializeField]
    Text timeToNextSpinText;
    [SerializeField]
    GameObject waitScreen;
    [SerializeField]
    GameObject blocker;
    [SerializeField]
    GameObject wheelPrices;
    [SerializeField]
    WheelSpin wheelSpinner;

    bool wheelSpinning = false;

    public int SpinLeft
    {
        get
        {
            return spinLeft;
        }
        set
        {
            if (value < 0)
            {
                waitScreen.SetActive(true);
                spinLeftText.text = "0";
                spinLeft = 0;
                return;
            }
            else
            {
                spinLeftText.text = value.ToString();
                waitScreen.SetActive(false);
            }
            spinLeft = value;
        }
    }


    // Use this for initialization
    void Start ()
    {
        SpinLeft = spinLeft;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.activeSelf && wheelSpinner.SpinSpeed == 0 &&
    !waitScreen.activeSelf && wheelSpinning)
        {
            BlockerClick(false);
            wheelSpinning = false;
        }
        if (!wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.activeSelf && wheelSpinner.SpinSpeed == 0 &&
            spinLeft == 0)
        {
            waitScreen.SetActive(true);
        }
        if (waitScreen.activeSelf)
        {
            timeToNextSpinText.text = (DateTime.Today.AddDays(1) - DateTime.Now).ToString();
        }

    }

    public void SpinWheel()
    {
        if (SpinLeft > 0)
        {
            blocker.SetActive(true);
            SpinLeft--;
            wheelSpinner.StartSpin();
            wheelSpinning = true;
        }
    }

    public void BlockerClick(bool stopwheel = false)
    {
        if (wheelSpinner.SpinSpeed != 0 && stopwheel)
        {
            wheelSpinner.SpinSpeed = 0;
            wheelSpinning = false;
        }
        if (!wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.activeSelf && wheelSpinner.SpinSpeed == 0)
        {
            wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.SetActive(true);
        }
        else if (wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.activeSelf && wheelSpinner.SpinSpeed == 0)
        {
            blocker.SetActive(false);
            wheelPrices.transform.GetChild((int)wheelSpinner.GetCurrentPrice()).gameObject.SetActive(false);
        }

    }
}
