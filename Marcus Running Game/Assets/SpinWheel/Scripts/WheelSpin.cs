﻿using UnityEngine;
using System.Collections;

public class WheelSpin : MonoBehaviour
{
    float spinSpeed;
    float slowSpeed;

    public float SpinSpeed
    {
        get
        {
            return spinSpeed;
        }
        set
        {
            if (value < 0)
            {
                spinSpeed = 0;
            }
            else
            {
                spinSpeed = value;
            }
        }
    }

    // Use this for initialization
    void Start ()
    {
        //StartSpin();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (spinSpeed > 0)
        {
            spinSpeed -= Time.deltaTime / slowSpeed;
            transform.Rotate(0, 0, spinSpeed);
        }
        else
        {
            spinSpeed = 0;
        }
        Debug.Log(GetCurrentPrice());

	}

    public WheelPrices GetCurrentPrice()
    {
        return (WheelPrices)this.GetComponent<WheelObjectHandler>().GetIconRotationIndex(this.transform.eulerAngles.z);
    }

    public void StartSpin()
    {
        spinSpeed = Random.Range(2.0f, 5.0f);
        slowSpeed = Random.Range(2.9f, 3.9999f);
   }
}


public enum WheelPrices
{
    PlusSpin = 0, MissSpin = 1, TenDiscount = 2, HundredCoins = 3,
}