﻿using UnityEngine;
using System.Collections;

public class ButtonPickup : Collectable
{
    public override void Start()
    {
    }

    public override void Update()
    {
    }

    public override void TriggerEnter2D(Collider2D col, Player player)
    {
        base.TriggerEnter2D(col, player);
        player.Score += Points;
        Destroy(this.gameObject);
    }
}
