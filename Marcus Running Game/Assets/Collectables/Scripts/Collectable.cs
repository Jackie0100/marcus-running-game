﻿using UnityEngine;
using System.Collections;

public abstract class Collectable : MonoBehaviour
{
    [SerializeField]
    protected int Points = 10;
	public virtual void Start()
    {
	
	}
	
	public virtual void Update()
    {
	
	}

    public virtual void TriggerEnter2D(Collider2D col, Player player)
    {
        player.collectableSounds.clip = player.point;
        player.collectableSounds.Play();
    }
}
