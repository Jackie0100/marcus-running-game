﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BackGroundHandler : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 2;
    [SerializeField]
    float offsetx = 0;
    [SerializeField]
    float offsety = 0;
    // Use this for initialization
    void Start()
    {
	
	}
	
	// Update is called once per frame
	void LateUpdate()
    {
        Vector3 vec = Camera.main.transform.position;
        transform.position = new Vector3((vec.x / moveSpeed) - offsetx, (vec.y / moveSpeed) - offsety);
	}
}
