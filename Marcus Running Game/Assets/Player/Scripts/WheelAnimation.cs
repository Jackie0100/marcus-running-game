﻿using UnityEngine;
using System.Collections;

public class WheelAnimation : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        this.GetComponent<Animator>().SetInteger("State", 0);
	}
	
    public void SpinWheelAnimation()
    {
        this.GetComponent<Animator>().SetInteger("State", 1);
        Invoke("WheelIdleAnimation", 0.5f);
    }

    public void WheelIdleAnimation()
    {
        this.GetComponent<Animator>().SetInteger("State", 2);
    }
}
