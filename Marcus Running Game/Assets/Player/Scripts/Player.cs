﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    public float health = 1;
    private int score = 0;
    public List<Collectable> Collectables;
    private bool leftButtonPressedDown = false;
    private bool rightButtonPressedDown = false;
    public bool isAlive = true;
    public bool isPaused = true;

    public AudioSource collectableSounds;
    public AudioSource playerSounds;
   

    [SerializeField]
    public AudioClip jump;

    [SerializeField]
    public AudioClip point;

    [SerializeField]
    public AudioClip slide;

    [SerializeField]
    public AudioClip death;

    [SerializeField]
    public AudioClip trampoline;

    [SerializeField]
    public GameObject WinGUI;
    [SerializeField]
    public GameObject GameOverGUI;

    public GameObject mainCamera;
    [SerializeField]
    private UnityEngine.UI.Text pointLabel;

    public bool isGrounded = true;

    private Controller controller;

    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            if (value <= 0)
            {
                health = 0;
                GameOverGUI.SetActive(true);
                playerSounds.clip = death;
                playerSounds.Play();
            }
            else
            {
                health = value;
            }
        }
    }

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            pointLabel.text = value.ToString();
        }
    }

    public Controller Controller
    {
        get
        {
            return controller;
        }
        set
        {
            //TODO: Change values and buttons animations etc.
            controller = value;
        }
    }

    void Awake()
    {
        Collectables = new List<Collectable>();
        controller = new RunnerController(false);
        if(mainCamera == null)
        {
            mainCamera = Camera.main.gameObject;
        }
        if (pointLabel == null)
        {
            Debug.LogError("Player Script: Point label is unassigned!");
        }
    }

	// Use this for initialization
	void Start()
    {
        controller.MoveCamera(this);
        playerSounds = mainCamera.AddComponent<AudioSource>();
        collectableSounds = mainCamera.AddComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update()
    {
        if (!isAlive || isPaused)
        {
            return;
        }
        //Score += 1;
        Controller.MovePlayer(this);
        controller.MoveCamera(this);
        if (Health <= 0)
        {
            //TODO: Gameover/reset
            isAlive = false;
            this.GetComponent<Animator>().SetInteger("State", 10);
            return;
        }
        if (!controller.FixedUpdate)
        {
            if ((leftButtonPressedDown && !rightButtonPressedDown && !controller.RightActionPlaying) || (controller.LeftActionPlaying))
            {
                controller.LeftAction(this);
            }

            if ((rightButtonPressedDown && !leftButtonPressedDown && !controller.LeftActionPlaying) || (controller.RightActionPlaying))
            {
                controller.RightAction(this);
            }
        }
	}

    void FixedUpdate()
    {
        if (!isAlive || isPaused)
        {
            return;
        }

        if (controller.FixedUpdate)
        {
            if ((leftButtonPressedDown && !rightButtonPressedDown && !controller.RightActionPlaying) || (controller.LeftActionPlaying))
            {
                controller.LeftAction(this);
            }

            if ((rightButtonPressedDown && !leftButtonPressedDown && !controller.LeftActionPlaying) || (controller.RightActionPlaying))
            {
                controller.RightAction(this);
            }
        }
    }

    void OnTrigger2DEnter(Collider2D col)
    {
        if (col.GetComponent<Collectable>() != null)
        {

            Collectables.Add(col.GetComponent<Collectable>());
            col.GetComponent<Collectable>().TriggerEnter2D(col, this);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Obsticle>() != null)
        {
            col.gameObject.GetComponent<Obsticle>().CollisionEnter2D(col, this);
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Floor" && !isGrounded)
        {
            isGrounded = true;
            this.GetComponent<Animator>().SetInteger("State", 1);
            controller.LeftActionPlaying = false;
            controller.RightActionPlaying = false;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Collectable>() != null)
        {
            col.GetComponent<Collectable>().TriggerEnter2D(col, this);
        }
        else if (col.tag == "FinishLine")
        {
            //TODO: Test if player stand still
            WinGUI.SetActive(true);
            //this.GetComponent<Animator>().enabled = false;
            this.GetComponent<Animator>().SetInteger("State", 0);
            this.enabled = false;
        }
    }


    public void ActionButtonLeftDown()
    {
        leftButtonPressedDown = true;
        controller.LeftButtonHeldDown = true;
    }

    public void ActionButtonLeftUp()
    {
        leftButtonPressedDown = false;
        controller.LeftButtonHeldDown = false;
        Controller.StopActionLeft(this);
    }

    public void ActionButtonRightDown()
    {
        rightButtonPressedDown = true;
        controller.RightButtonHeldDown = true;
    }

    public void ActionButtonRightUp()
    {
        rightButtonPressedDown = false;
        controller.RightButtonHeldDown = false;
        Controller.StopActionRight(this);
    }
}
